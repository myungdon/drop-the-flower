'use client'

import {useSelector, useDispatch} from "react-redux";
import {resetFlower, mixingFlower, pickFlower} from "@/lib/features/flower/flowerSlice";
import axios from "axios";
import { useState } from "react";

export default function Home() {
    const [response, setResponse] = useState('')

    const dispatch = useDispatch()
    const {mixFlowers, pickFlowers} = useSelector((state) => state.flower)

    const handleMix = () => {
        dispatch(mixingFlower())
    }

    const handlePick = (flowerName) => {
        if (pickFlowers.length < 2) {
            dispatch(pickFlower(flowerName))
        } else {
            alert('최대 두 장의 카드만 선택할 수 있습니다.')
        }
    }

    const handleReset = () => {
        dispatch(resetFlower())
        setResponse('')
    }

    const handleConfirm = () => {
        if (pickFlowers.length === 2) {
            const meaning = pickFlowers.flatMap(flower => flower.meaning).join(', ')
            const command = `${meaning}의 단어들을 조합해서 5줄 정도의 오늘의 운세를 만들어 줘`

            const apiUrl = 'https://generativelanguage.googleapis.com/v1/models/gemini-1.5-flash:generateContent?key=AIzaSyB_P_HcSmI7Qh1fgAUa8YQjE4noRNW2abU'

            const data = {"contents":[{"parts":[{"text": command}]}]}
            axios.post(apiUrl, data)
                .then(res => {
                    setResponse(res.data.candidates[0].content.parts[0].text)
                })
                .catch(err => {
                    console.log(err)
                })
        }
    }

    return (
        <main className="text-center">
            <div>
                <h1 className="text-5xl mt-10 mb-10">오늘의 운세</h1>
                {mixFlowers.length === 0 ? (
                    <button onClick={handleMix} className="">썪기</button>
                ) : (
                    <div>
                        <h1>선택하세요</h1>
                        <div className="flowers">
                        {mixFlowers.map((flower) => (
                            <div key={flower.name} onClick={() => handlePick(flower.name)}>
                                {flower.selected ? (
                                    <div>
                                        <img src={`/assets/${flower.imgName}`} alt={flower.name} width="200px"/>
                                        <p className="mb-20">{flower.name}</p>
                                    </div>
                                ) : (
                                    <div>
                                        <img src='/assets/back.png' alt="back" className="mb-20" width="200px"/>
                                    </div>
                                )}
                            </div>
                        ))}</div>
                        {pickFlowers.length === 2 && (
                            <div>
                                <button onClick={handleConfirm} className="mr-28">운세보기</button>
                                <button onClick={handleReset} className="mb-20">다시하기</button>
                                {response && <p className="ml-96 mr-96 mb-20">{response}</p>}
                            </div>
                        )}
                    </div>
                )}
            </div>
        </main>
    )
}