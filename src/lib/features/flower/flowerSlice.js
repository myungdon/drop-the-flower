import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    flowers: [
        { name: '1월', imgName: '1.png', meaning: ['소식', '손님', '남자']},
        { name: '2월', imgName: '2.png', meaning: ['님', '여자']},
        { name: '3월', imgName: '3.png', meaning: ['외출', '혼란', '만남']},
        { name: '4월', imgName: '4.png', meaning: ['싸움', '무관심']},
        { name: '5월', imgName: '5.png', meaning: ['결혼', '이성']},
        { name: '6월', imgName: '6.png', meaning: ['기쁨', '호감']},
        { name: '7월', imgName: '7.png', meaning: ['행운', '돈']},
        { name: '8월', imgName: '8.png', meaning: ['어둠', '저녁']},
        { name: '9월', imgName: '9.png', meaning: ['술']},
        { name: '10월', imgName: '10.png', meaning: ['근심', '풍파', '바람']},
        { name: '11월', imgName: '11.png', meaning: ['돈', '복']},
        { name: '12월', imgName: '12.png', meaning: ['손님', '눈물']}
    ],
    mixFlowers: [],
    pickFlowers: []
}

const flowerSlice = createSlice({
    name: 'flower',
    initialState,
    reducers: {
        resetFlower: (state) => {
            state.mixFlowers = []
            state.pickFlowers = []
        },
        mixingFlower: (state) => {
            state.mixFlowers = [...state.flowers].map(flower => ({...flower, selected: false}))
            for(let i = state.mixFlowers.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                [state.mixFlowers[i], state.mixFlowers[j]] = [state.mixFlowers[j], state.mixFlowers[i]];
            }
        },
        pickFlower: (state, action) => {
            const flowerIndex = state.mixFlowers.findIndex(flower => flower.name === action.payload);
            if (flowerIndex !== -1) {
                state.mixFlowers[flowerIndex].selected = true;
                state.pickFlowers.push(state.mixFlowers[flowerIndex])
            }
        }
    }
})

export const {resetFlower, mixingFlower, pickFlower} = flowerSlice.actions

export default flowerSlice.reducer