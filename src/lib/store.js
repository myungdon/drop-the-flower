import { configureStore } from '@reduxjs/toolkit'
import flowerReducer from "@/lib/features/flower/flowerSlice";

export default configureStore({
    reducer: {
        flower: flowerReducer
    },
})